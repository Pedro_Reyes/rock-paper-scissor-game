package com.game.rock_paper_scissors.util;

import java.io.IOException;

import com.fasterxml.jackson.databind.ObjectMapper;

public class JsonMapperUtil {
	
	/**
	 * Given a POJO object, it will return the JSON value as a String
	 * 
	 * @param gameReport
	 * @return
	 */
	public static String getJsonFromObject(Object object) {
		// Creating Object of ObjectMapper define in Jakson Api
		ObjectMapper Obj = new ObjectMapper();

		String jsonStr = null;
		try {
			jsonStr = Obj.writeValueAsString(object);
		} catch (IOException e) {

		}

		return jsonStr;
	}

}
