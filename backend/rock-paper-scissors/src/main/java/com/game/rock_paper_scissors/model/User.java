package com.game.rock_paper_scissors.model;

public class User {

	private String uid = null;
	
	public User() {
		
	}
	
	public User(String uid) {
		this.uid = uid;
	}

	public String getUid() {
		return uid;
	}

	public void setUid(String uid) {
		this.uid = uid;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return true;
		}

		if (obj == null || obj.getClass() != this.getClass())
			return false;

		User anotherUser = (User) obj;

		return uid.equals(anotherUser.uid);
	}

}
