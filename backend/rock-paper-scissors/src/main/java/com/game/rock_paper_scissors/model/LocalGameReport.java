package com.game.rock_paper_scissors.model;

import java.util.ArrayList;
import java.util.List;

public class LocalGameReport {

	private List<Round> roundFromCurrentGame;
	
	public LocalGameReport() {
		this.roundFromCurrentGame = new ArrayList<Round>();
	}

	public List<Round> getRoundFromCurrentGame() {
		return roundFromCurrentGame;
	}

	public void setRoundFromCurrentGame(List<Round> roundFromCurrentGame) {
		this.roundFromCurrentGame = roundFromCurrentGame;
	}

}
