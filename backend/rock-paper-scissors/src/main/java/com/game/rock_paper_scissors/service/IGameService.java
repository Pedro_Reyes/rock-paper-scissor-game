package com.game.rock_paper_scissors.service;

import com.game.rock_paper_scissors.model.User;

public interface IGameService {

	/**
	 * Associates to the user ID given a new round
	 * 
	 * @param uid
	 * @return
	 */
	public String playRound(User user);

	/**
	 * Current user id rounds are reset in order to be detected as round that are
	 * not part of the current game
	 * 
	 * @param uuid
	 * @return
	 */
	public String restartGame(User user);

	/**
	 * A report of all the rounds played is returned
	 * 
	 * @return
	 */
	public String generateGlobalGameReport();

	/**
	 * Generate a User ID for a particular game played by a user
	 * 
	 * @return
	 */
	public String generateUID();
}
