import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GlobalGameReportComponent } from './global-game-report.component';

describe('GlobalGameReportComponent', () => {
  let component: GlobalGameReportComponent;
  let fixture: ComponentFixture<GlobalGameReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GlobalGameReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GlobalGameReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
