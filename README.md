# To the interviewer
This repository is the merge of the backend and fronted in one repository. In this repository the commits are made based on the two projects already done. For the purpose of seeking the process I went through every commit from scratch please refer to the projects separately:

- Fronted: https://bitbucket.org/Pedro_Reyes/rock-paper-scissor-game-frontend/src/master/
- Backend: https://bitbucket.org/Pedro_Reyes/rock-paper-scissor-game-backend/src/master/ 

# Intro

Technical assessment description:

![technical_assessment](images/technical_assessment.png)

The project has been developed using:

- Springboot 2.1.0.RELEASE for the backend and 
- Angular 8.2.9 for the fronted

As required no database whatsoever has been used. The data structure is based on a ConcurrentHashMap that makes a relationship between UID and a SynchronizedArrayList of Round objects. The UID is a temporal session identifier for the current user playing.

# Pre-requisites
You have to install:

- Git: https://git-scm.com/book/en/v2/Getting-Started-Installing-Git
- Node.js: https://nodejs.org/es/
- Angular: https://cli.angular.io/
- Angular Material: https://material.angular.io/guide/getting-started
- Maven: https://www.baeldung.com/install-maven-on-windows-linux-mac
- Eclipse IDE or SpringToolSuite4: https://spring.io/tools

# Deployment

Once everything is installed:

1. Import the project in the IDE and run it as a Java Application
2. Go to the angular project from a terminal, execute 'npm install', and afterwards execute 'ng serve'.
3. You can call the fronted in this URL: http://localhost:4200/
4. You can call the backend in this URL: 
4.1. http://localhost:8080/game/generate-uid
4.1. http://localhost:8080/game/play-round
4.1. http://localhost:8080/game/restart
4.1. http://localhost:8080/game/report

# System working

![server_not_working](images/server_not_working.png)

![server_working](images/server_working.png)

![server_working_with_rounds_played](images/server_working_with_rounds_played.png)

# Author

Pedro Jesús Reyes Santiago - https://www.linkedin.com/in/pedro-jesus-reyes-santiago-54a199a6/